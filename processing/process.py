import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

DATAFILE="../data/valgrind.tsv"

measurements = []

def plot(data, bincount, name):
    hist, bins = np.histogram(data, bins=bincount)
    plt.bar(bins[:-1], hist.astype(np.float32) / hist.sum(), width=(bins[1]-bins[0]))
    csfont = {'fontname': 'roboto', 'fontweight': 'regular'}
    plt.title(name, **csfont)
    plt.xlabel("Relative instruction count", **csfont)
    plt.ylabel("Frequency", **csfont)
    plt.grid(True)
    tikz_save('../graphics/{}.tex'.format(name.lower()))
    plt.close("all")

with open(DATAFILE, 'r') as f:
    for line in f:
        if line.startswith("FilterThread"):
            continue
        linesplitted = line.split()
        if len(linesplitted) == 0 :
            continue
        measurements.append({"fil": float(linesplitted[0]),
                             "dem": float(linesplitted[1]),
                             "src": float(linesplitted[2]),
                             "rsmp": float(linesplitted[3]),
                             "enc": float(linesplitted[4]),
                             "store": float(linesplitted[5])})

plot([measurement['fil'] for measurement in measurements], 50, "Filter")
plot([measurement['dem'] for measurement in measurements], 50, "Demodulator")
plot([measurement['src'] for measurement in measurements], 50, "Source")
plot([measurement['rsmp'] for measurement in measurements], 25, "Resampler")
plot([measurement['enc'] for measurement in measurements], 25, "Encoder")
plot([measurement['store'] for measurement in measurements], 25, "Store")
