\section{Method}
\label{sec:method}

\subsection{Platform}
The target platform is the multiprocessor system-on-chip (MPSoC) provided for the RTS2 course.
Attempts to compile the created C++ libraries for this platform, however, failed.
With a more recent toolchain, it should be possible to adapt the application for the MPSoC platform.
Because of this the application is currently only viable on platforms with better compiler support such as x86 or ARM.
Section~\ref{sub:perf} details the performance analysis done for each platform.

\subsection{Model}

\begin{figure}[ht]
  \centering
  \includegraphics{pipeline.dot.eps}
  \caption{Model of two parallel SDR pipelines.}
  \label{fig:sdr-pipeline}
\end{figure}

The khan Process Network (KPN) of the application is shown in Figure~\ref{fig:kpn}.
The model of the application is as follows:
a sampler provides the samples for each pipeline in the main memory.
This allows multiple pipelines to make use of the samples.
Figure~\ref{fig:sdr-pipeline} shows two SDR pipelines,
but the desired application has as many SDR pipelines as there are clients.

The pipeline is split into two parts: radio and audio.
These two parts have different deadline requirements,
which are discussed in section~\ref{sub:radio} and~\ref{sub:audio} respectively.

\subsection{Thread communication}
Thread communication is implemented via blocking, lockless FIFOs.
The FIFOs are implemented by the \emph{readerwriterqueue} library\footnote{\url{https://github.com/cameron314/readerwriterqueue}}.
The capacity of these FIFOs is not limited by a fixed size, and the implementation will not work on the MPSoC.
Having non-limited capacity, while unrealistic for the target implementation, allows for separate analysis of process performance.
This FIFO implementation can be replaced by the FIFO implementation of the MPSoC when the application is implemented on this platform.

\subsection{Radio}
\label{sub:radio}
The part of the pipeline responsible for processing the radio is set up with the following tasks:

\begin{enumerate}
  \item Source
  \item Filter
  \item Demodulator
  \item Resampler
\end{enumerate}

When a part of this pipeline does not perform within the deadline on one of the platforms,
the application can be adapted in order to distribute the samples to a second parallel radio pipeline.
A second radio pipeline would also execute the Filter, Demodulater and Resampler tasks in sequence,
joining with the primary pipeline afterwards.

\subsubsection{Source}
\label{sub:source}
The \emph{Source} task is responsible for taking the right samples from the main memory and providing them on a FIFO within the deadline.
Separating this task allows the other tasks to only depend on the input and output FIFO.
This task can also be modified to fit the method of retrieving the samples from main memory, which is initially done by creating them in advance.
This task needs to provide the samples at the radio sample rate $f_{s,radio}$.

In order to test the behaviour of the pipeline, samples can be pre-generated.
This is implemented in two ways:

\begin{itemize}
  \item By generating white Gaussian noise.
    While there is no information encoded in this noise, the performance of the demodulating and encoding the noise is expected to perform as slow as possible.
  \item By generating simple sinusoid signals and modulating them.
    This is done once for each channel with a different signal for every channel. This implementation allows for easy testing and debugging of the entire pipeline.
\end{itemize}

\subsubsection{Filter}
The \emph{Filter} task applies a band-pass filter tuned to the current channel.
This task has to perform at at least $f_{s,radio}$.

The implemented filter is an infinite-impulse-response (IIR) filter from the liquid-dsp library\footnote{\url{http://liquidsdr.org/doc/iirfilt/}}.
This is a 5th order Elliptic (Cauer) filter with \SI{60}{dB} attenuation outside the pass-band.

\subsubsection{Demodulator}
The \emph{Demodulator} task demodulates the AM signal on the channel.
This task has to perform at at least $f_{s,radio}$.

The implemented demodulator is a dual-sideband AM (de)modulator from the liquid-dsp library\footnote{\url{http://liquidsdr.org/doc/ampmodem/}}.

\subsubsection{Resampler}
The \emph{Resampler} task resamples the demodulated samples from the radio sampling rate to the audio sampling rate.
This task receives data at $f_{s,radio}$ and provides data at the audio sample rate $f_{s,audio}$.

For this Resampler, the arbitrary resampler from liquid-dsp is used\footnote{\url{http://liquidsdr.org/doc/resamp/}}.
While this filter is meant for arbitrary (irrational) resampling ratios,
it is also the only available resampler that allows for an integer resampling ratio that is not a power of two.

An alternative implementation would be to resample by discarding a set number of samples.

\subsection{Audio}
\label{sub:audio}
The part of the pipeline responsible for processing the radio is setup with the following tasks:

\begin{enumerate}
  \item Encode
  \item Store (or Transmit)
\end{enumerate}

\subsubsection{Encode}
The \emph{Encode} taks encodes the audio samples with the Opus codec.
This tasks receives data at $f_{s,audio}$ and transmits at a variable rate.

For efficient encoding, a number ($N$) of (PCM encoded) samples are stored in a buffer and encoded at the same time.
The amount of data ($n$) generated is not known before encoding, but is at worst $N$.
This data is provided to the Store thread on creation.

\subsubsection{Store/Transmit}
The purpose of this thread is to simulate the transmitter.
For testing, this simply stores the generated data to memory.

In the target application this would create a UDP packet and transmit this to the intended receiver.

\subsection{Application parameters}
The only (relevant) audio sample rates are \SI{24}{kHz} and \SI{48}{kHz} (table~\ref{tab:opus:samplerates}).
Because an integer resampling ratio is preferred, the radio sampling rate is chosen at \SI{576}{kHz} ($24\times24$ or $48\times12$).
Additionally, because listeners are most-likely audiophiles, the audio sampling rate is chosen at \SI{48}{kHz} and a target bitrate of \SI{64}{kb/s}.
This can be changed if the encoding thread does not meet its deadlines.

The chosen parameters are shown in table~\ref{tab:params}.

\begin{margintable}
  \centering
  \begin{tabular}{l r}
    \toprule
    Parameter & Value \\
    \midrule
    $f_{s,radio}$  & \SI{576}{kHz} \\
    $f_{s,audio}$  & \SI{48}{kHz} \\
    Resample ratio & 12 \\
    Opus bitrate   & \SI{64}{kB/s} \\
    $N$            & 120 \\
    \bottomrule
  \end{tabular}
  \caption{Chosen parameters for the SDR application.%
    \label{tab:params}}
\end{margintable}

\subsection{Performance analysis}
\label{sub:perf}
Task execution times are required to calculate the FIFO buffer sizes.
These buffer capacities must be sufficient to operate under worst-case loads since Sodd's second law states:

\begin{quote}
  Sooner or later, the worst possible combination of circumstances will happen.
\end{quote}

Worst-case execution times (WCETs) are often used to represent this scenario\cite{rts-book} and is found during worst-case runtime testing.
An overestimation of the WCET is used to define the execution time of a task.
The overestimation results in a reliable but pessimistic estimate of the minimum throughput,
assuming the test program covers the worst-case scenarios.

Because attempts to run the application on the MPSoC platform were unsuccessful,
the analysis is done in two parts:
i) the upper bound for performance on the MPSoC is analysed to establish if the application is feasible,
and ii) the actual and worst-case performance on a multi-core general purpose processor (GPP) is determined to analyse the requirements of the pipeline.

\subsection{FIFO}
The current x86 implementation uses FIFOs without fixed size.
While the application is currently not viable for the Starburst MPSoC,
it is still important to know what the buffer sizes would need to be in order to run the application on it.
The buffer sizes are calculated analytically by:

\begin{itemize}
	\item Determining the critical cycle using the WCETs found by the performance analysis.
	\item Setting the variable production rate of the encoder to the worst-case scenario.
\end{itemize}

The results are used to reason about the viability of the FIFO capacity:
in order to achieve maximum performance,
tasks should never take longer than the critical cycle.
This includes waiting on the FIFO.

\subsection{MPSoC}
The upper bound for performance is estimated by testing the multiplication of two floating point numbers
and communicating the input and result via FIFO.
This is done in a similar pipeline setup to the actual application.
Performance is measured by doing a fixed number of tests and measuring the time taken.

\subsection{GPP}
The performance of the entire pipeline is estimated by running the application,
which measures the time taken to execute the complete pipeline with the generated samples.

The application is analysed using the \emph{callgrind} tool from \emph{valgrind}\footnote{\url{http://valgrind.org/}},
which provides the \emph{instruction count} of every function called.
While instruction count does not directly map to actual execution time,
the relative instruction count of every thread reflects this more directly.
Analysing the instruction count also has the benefit that it is not influenced by other processes running on the system.
Furthermore, this does not account for uncertainty due to caches.
These have to be taken into account manually by adding uncertainty to the execution times.

In order to estimate the WCET of every task,
the application is tested 500 times with random noise as input data (see section~\ref{sub:source}).
From the maximum of all these results and the distribution of these results the WCET can be estimated.

Linux provides a sampling based framework to profile applications which can also be used to analyse the timings of tasks.
The maximum sampling frequency of this framework, however, is \SI{1}{kHz}.
Although useful to measure timings of different stages of the pipeline,
the limited granularity of the sampling frequency makes measurements on the high speed components of the pipeline difficult.
The advantage of using profiling based measurements is that deviations due to caches and other unpredicable components can be determined.
Another problem with this approach is that a lot of postprocessing is necessary to subtract FIFO wait times.

Due to the limited sampling rate and the postprocessing needed before accurate thread timings are possible, this profiling method is not suitable for our application.

\begin{figure}
  \centering
  \includegraphics{kpn.dot.eps}
  \caption{KPN model of the SDR pipeline.}
  \label{fig:kpn}
\end{figure}
