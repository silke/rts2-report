\section{Execution}

\begin{margintable}
  \centering
  \begin{tabular}{l l}
    \toprule
    Architecture & x86 \\
    Brand        & Intel \\
    Model        & Core i5-3570K \\
    Clock speed  & 3.40 - 3.80 GHz \\
    Cores        & 4 \\
    \bottomrule
  \end{tabular}
  \caption{Specifications of the processor on which the application is tested.%
    \label{tab:gpp}}
\end{margintable}

The application is executed on a GPP (detailed in table~\ref{tab:gpp}) analysed as described in section~\ref{sec:method}.
Table~\ref{tab:relinstructions} shows the instruction count relative to the task with the lowest WCET.
The WCET is estimated at the sum of the maximum value and three times the standard deviation to account for uncertainty.
For every task, the distribution of instruction counts is also shown
(figure~\ref{fig:hist:source}, \ref{fig:hist:filter}, \ref{fig:hist:demodulator},
\ref{fig:hist:resampler}, \ref{fig:hist:encoder}, and \ref{fig:hist:store}).
These histograms confirm that the WCET-times are indeed a pessimistic worst-case estimate of the execution times of the tasks.

\begin{marginfigure}
  \centering
  \includegraphics{activity}
  \caption{Graph of the CPU usage of the host system when running the application.%
    \label{fig:activity}}
\end{marginfigure}

On the GPP, the pipeline is able to process about \SI{5228}{kSamples/s} on average.
The performance is significantly influenced by the system load.
Figure~\ref{fig:activity} shows that all processor cores are used when running the application.
This demonstrates the effective multi-threaded nature of the application.

Figure \ref{fig:vrdf} shows the FIFO sizes calculated for the pipeline with the found WCETs.
The filter task contains the critical cycle that bottlenecks the system,
with the demodulator task being only marginally faster.
The buffer capacity of the FIFO between the source and filter, and between the filter and demodulator is set at 2 in order to achieve maximum performance.
This allows for the tasks to continue when a token is still waiting at the FIFO.
Between the demodulator and resampler the capacity is set to 24,
allowing the demodulator to continue to fill the buffer while the resampler is in operation.
The FIFO capacity between the resampler and the encoder, and the encoder and transmitter is set at $N$ (120).
In the worst case, the blocking of these tasks by the FIFO capacity does not take longer than the critical cycle.

\begin{figure}
	\centering
	\includegraphics{srd-hapi-mcm-bw.png}
	\caption{Hapi simulation of SDF graph (T = 1 ms)}
	\label{fig:hapi}
\end{figure}

The VRDF was converted to an SDF for the worst case scenario (n=120) and simulated with Hebe and Hapi.
The Hebe simulation of the resulting SDF graph calculates a Maximum Cycle Mean (MCM) of 299520 relative instructions.
This corresponds to the rounded execution time of the filter multiplied with the amount of executions needed for one cycle of the pipeline:
\[208\times12\times120 = 299520\]
This confirms that the self-loop of the filter task is the critical cycle.
The transition phase is found to be 105 relative instructions and hence the first cycle of the pipeline ends at 299625 (\(299520+105\)).
The Hapi simulation shown in figure~\ref{fig:hapi} shows that at that time the filter finishes execution.
The simulation also shows that the encoder and transmit tasks do not block the other tasks.

The upper limit of the MPSoC, measured with five million samples, is measured at \SI{251.1}{kSamples/s}.
This means that at least three parallel pipelines are required in order to process more samples per second than required by the radio sampling rate.

\vfill

\begin{figure*}[ht]
  \centering
  \includegraphics[bb=36 316 980 476]{vrdf.dot.pdf}
  \margincaption{VRDF model of the SDR application.%
    \label{fig:vrdf}}
\end{figure*}

\begin{table}[ht]
  \centering
  \sisetup{round-mode=places,round-precision=2}
  \begin{tabular}{l l l l l}
    \toprule
    Thread & Mean & Max & Stdev. & WCET \\
    \midrule
    Source      & \num{104.06831530561729} & \num{104.07115359184861} & \num{0.010186506635245495} & \num{104.10171311175435}  \\
    Filter      & \num{204.58687174335114} & \num{207.18655603139794} & \num{0.24317010922862095 } & \num{207.9160663590838 }  \\
    Demodulator & \num{197.44632096097058} & \num{202.00677599147   } & \num{0.48460566551070444 } & \num{203.4605929880021 }  \\
    Resampler   & \num{101.84844749063946} & \num{104.72187381054718} & \num{0.42619115509779365 } & \num{106.00044727584056}  \\
    Encoder     & \num{9.638437200613364 } & \num{12.206899529393683} & \num{0.4513855444233067  } & \num{13.5610561626636  }  \\
    Store       & \num{0.3273097284562662} & \num{0.6439642020706254} & \num{0.11867859930979148 } & \num{1.0               }  \\
    \bottomrule
  \end{tabular}
  \caption{Relative instruction count of every task.}
  \label{tab:relinstructions}
\end{table}

\histogram{source}
\histogram{filter}
\histogram{demodulator}
\histogram{resampler}
\histogram{encoder}
\histogram{store}
