\section{Theory}

\subsection{Long wave radio}
Long wave radio refers to radio with relatively long wavelengths,
usually longer than 1000 metres.
In International Telecommunication Union (ITU) region 1 (Europe, Africa and parts of Asia) this band is defined between 153 and 279 kHz,
consisting of 14 channels with frequencies that are multiples of 9 kHz.

The modulation scheme used in long wave radio is A3E, the ITU designation for double-sideband full-carrier.

Long wave radio signals can, depending on atmospheric conditions, be received up to 2.000 km from the transmitting antenna.

\subsection{Radio reception}
To turn the radio signals into raw audio signals, three things need to happen:

\begin{enumerate}
  \item Filtering: the desired channel needs to be filtered out of the raw samples with a band pass filter.
  \item Demodulation: the AM-modulated signal needs to be demodulated,
    after which the raw audio is obtained.
  \item Resampling: the radio is sampled at a rate which is higher than required (or allowed) for audio signals.
\end{enumerate}

The sampling frequency ($f_s$) required to reconstitute the full long wave radio spectrum ($f_{max} = \SI{279}{kHz}$), can be calculated with Nyquist criterion:

\begin{align*}
  f_s &> 2B \\
      &> 2 \cdot \SI{279}{kHz} \\
      &> \SI{558}{kHz}
\end{align*}

\subsection{Audio}
Audio signals reflect sound waves.
The range of frequencies that humans can hear is roughly 20 to \SI{20000}{Hz},
resulting in a required sampling frequency of at least \SI{40}{kHz} to cover all audible sounds.

Digital audio signals are usually PCM (pulse-code modulation) encoded:
samples from an analog-to-digital converter are directly encoded into a bit stream.
The sample depth (number of bits per sample) determines to what extend each sample is quantised.

Audio signals are usually compressed with an audio codec before being stored or sent.
While there are some lossless codecs, the ones obtaining the highest compression ratio are lossy.
Some examples of such codecs are MP3, AAC, and Opus.

\subsubsection{Opus}

\begin{margintable}
  \centering
  \begin{tabular}{l r r}
    \toprule
    Abbreviation & B (kHz) & $f_s$ (kHz)\\
    \midrule
    NB  (narrowband)      &  4 &  8 \\
    MB  (medium-band)     &  6 & 12 \\
    WB  (wideband)        &  8 & 16 \\
    SWB (super-wideband)  & 12 & 24 \\
    FB  (fullband)        & 20 & 48 \\
  \end{tabular}
  \caption{Supported sample rates of the Opus codec\cite{opus-rfc}.%
    \label{tab:opus:samplerates}}
\end{margintable}

\begin{margintable}
  \centering
  \begin{tabular}{l l}
    \toprule
    Bitrate (kbit/s) & Target \\
    \midrule
    8-12   & NB speech \\
    16-20  & WB speech \\
    28-40  & FB speech \\
    48-64  & FB mono music \\
    64-128 & FB stereo music \\
  \end{tabular}
  \caption{Bitrate targets of the Opus codec\cite{opus-rfc}.%
    \label{tab:opus:bitrates}}
\end{margintable}

Opus is a lossy audio coded designed for low-latency and adaptive complexity\cite{opus-rfc}.
This allows Opus to be used for real-time applications (like this one) on (slower) embedded processors.
The main libraries for encoding audio with Opus are free/libre and open source software (FLOSS).

The Opus codec supports several sample rates (table~\ref{tab:opus:samplerates})
and a wide range of bitrates (table~\ref{tab:opus:bitrates}).
